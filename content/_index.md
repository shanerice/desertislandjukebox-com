## The Playlist

This is a playlist for GitLab team members to share their one pick for a song to listen to for years if they were stranded on a desert island.

{{< youtube "playlist?list=PLRkOSEmxk9c07bxb0ZUAcu03pK7pcI1MY" >}}

Direct link to [YouTube Playlist](https://www.youtube.com/playlist?list=PLRkOSEmxk9c07bxb0ZUAcu03pK7pcI1MY)

To add your song to the YouTube playlist email your choice to jukebox at shanerice dot com or [fill out this form](https://docs.google.com/forms/d/e/1FAIpQLSeJ7PMqKA91cxBnmthpjXYC7oZSx7vmDZ91gja-HTDB3yMzmA/viewform?usp=sf_link) to get a collaboration link.

You can [view this project on GitLab](https://gitlab.com/shanerice/desertislandjukebox-com/).

This playlist is not endorsed or curated by GitLab.
